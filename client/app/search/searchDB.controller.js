(function () {
    angular
        .module("EMS")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['$state','EmpService'];

    function SearchDBCtrl($state, EmpService) {
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showDepartment = false;

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.goEdit = goEdit;
        vm.search = search;
        vm.searchForDepartment = searchForDepartment;

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        init();



        // Function declaration and definition -------------------------------------------------------------------------
        function goEdit(empNo){
            $state.go("editWithParam",{empNo : empNo});
        }

        // The init function initializes view
        function init() {
            // We call EmpService.retrieveEmpDB to handle retrieval of department information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available departments, thus we ask service to retrieve '' (i.e., match all)
            EmpService
                .retrieveEmpDB('')
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.employees = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        // The search function searches for employees that match query string entered by user. The query string is
        // matched against the employee first name, last name, and employee number.
        function search() {
            vm.showDepartment = false;
            EmpService
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveEmpDB(vm.searchString)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.employees = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        // The search function searches for departments that matches query string entered by user. The query string is
        // matched against the employee name and employee number alike.
        function searchForDepartment() {
            vm.showDepartment = true;
            EmpService
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveEmpDept(vm.searchString)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.employees = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.info("error " + JSON.stringify(err));
                });
        }
    }
})();