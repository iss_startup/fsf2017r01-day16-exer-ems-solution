// TODO: Search(specific), Update, Delete
// TODO: 3. Build controller for edit html. Should support functionalities listed in edit.html
// Always use an IIFE, i.e., (function() {})();

(function () {

    angular
        .module("EMS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter","$stateParams", "EmpService"];

    function EditCtrl($filter, $stateParams, EmpService) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.emp_no = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.deleteEmp = deleteEmp;
        vm.initDetails = initDetails;
        vm.updateEmpName = updateEmpName;
        vm.search = search;
        vm.toggleEditor = toggleEditor;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();
        console.log("before state");
        if($stateParams.empNo){
            console.log("$stateparams " + $stateParams.empNo);
            vm.emp_no = Number($stateParams.empNo);
             vm.search();
        }


        // Function declaration and definition -------------------------------------------------------------------------
        // Deletes displayed manager. Details of preceding manager is then displayed.
        function deleteEmp() {
            EmpService
                .deleteEmp(vm.emp_no)
                .then(function (response) {
                    // Calls search() in order to populate manager info with predecessor of deleted manager
                    search();
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error: \n" + JSON.stringify(err));
                });
        }

        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.emp_no = "";
            vm.result.first_name = "";
            vm.result.last_name = "";
            vm.result.empdept_no = "";
            vm.showDetails = false;
            vm.isEditorOn = false;

        }

        // Saves edited department name
        function updateEmpName() {
            console.log("-- show.controller.js > updateEmpFirstName()");
            EmpService
                .updateEmpName( vm.result.emp_no, vm.result.first_name, vm.result.last_name)
                .then(function (result) {
                    console.log("-- show.controller.js > updateEmpFirstName() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > updateEmpFirstName() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }


        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
            initDetails();
            vm.showDetails = true;

            EmpService
                .retrieveEmpByNo(vm.emp_no)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.emp_no = result.data.emp_no;
                    vm.result.first_name = result.data.first_name;
                    vm.result.last_name = result.data.last_name;
                    vm.result.empdept_no = result.data.dept_emps[0].dept_no;
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });

        }
        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

    }
})();