//TODO: Add ability to choose department when registering an employee.-->
//TODO: Saving of employee in employees table and of department in departments table should be-->
//TODO: one  transaction (i.e., atomic)-->
//TODO: 2. Allow users to choose a department from a list of employees. Note: Function to call is in DeptService


// Always use an IIFE, i.e., (function() {})();
(function() {
    angular
        .module("EMS")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
        // this syntax is called the getter syntax
        .controller("RegCtrl", RegCtrl);    // angular.controller() attaches a controller to the angular module specified
                                            // as you can see, angular methods are chainable

    // TODO: 2.1 Inject DeptService so we can access appropriate function, Inject $filter because we need it to format dates
    RegCtrl.$inject = [ '$window', 'EmpService', 'DeptService' ];

    function RegCtrl( $window, EmpService, DeptService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the RegCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of RegCtrl, e.g., index.html
        var vm = this;
        var today = new Date();
        var birthday = new Date();
        birthday.setFullYear(birthday.getFullYear() - 18);

        // Exposed data models ---------------------------------------------------------------------------------------
        // Creates an employee object that
        // We expose the employee object by attaching it to the vm
        // This will allow us apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.employee = {
            empNo: "",
            firstname: "",
            lastname: "",
            gender: "M",
            birthday: birthday,
            hiredate: today,
            phonenumber: "",
            department: ""
        };

        // Creates a status object. We will use this to display appropriate success or error messages.
        vm.status = {
            message: "",
            code: ""
        };


        vm.register = register;

        // TODO: 2.2 Create function that would populate the department selection box with data. Logic should be placed
        // TODO: 2.2 in DeptService, but handling of success/error should be done in this controller

        initDepartmentBox();

        // Exposed functions -----------------------------------------------------------------------------------------

        function initDepartmentBox() {
            DeptService
                .retrieveDepartments()
                .then(function (results) {
                    console.log("--- Departments ----");
                    console.log(results.data);
                    vm.departments = results.data;
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.code = err.data.parent.errno;
                });
        }

        // Function declaration and definition
        function register() {
            // Calls alert box and displays registration information
            alert("The registration information you sent are \n" + JSON.stringify(vm.employee));

            // Prints registration information onto the client console
            console.log("The registration information you sent were:");
            console.log("Employee Number: " + vm.employee.empNo);
            console.log("Employee First Name: " + vm.employee.firstname);
            console.log("Employee Last Name: " + vm.employee.lastname);
            console.log("Employee Gender: " + vm.employee.gender);
            console.log("Employee Birthday: " + vm.employee.birthday);
            console.log("Employee Hire Date: " + vm.employee.hiredate);
            console.log("Employee Phone Number: " + vm.employee.phonenumber);
            console.log("Employee Department name: " + vm.employee.department);


            // We call EmpService.insertEmp to handle registration of employee information. The data sent to this
            // function will eventually be inserted into the database.
            EmpService
                .insertEmp(vm.employee)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });

        } // END function register()
    } // END RegCtrl
})();